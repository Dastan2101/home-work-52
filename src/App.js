import React, { Component } from 'react';
import Movie from './MovieComponent';
import Header from './header_component';
import img01 from './img/Black_Panther_film_poster.jpg';
import img02 from './img/Avengers_Infinity_War_poster.jpg';
import img03 from './img/Постер_фильма_Тихое_место.jpg';
import img04 from './img/missiya-no-vypolnimos.jpeg';
import img05 from './img/images.jpeg';
import img06 from './img/shelkunchik.jpg';
import SideBar from './side_bar_component';
import Footer from './footer_component';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Header/>
          <div className="main-div">
              <div className="main-bar">
                  <h3 className="title">Категории</h3>
                  <SideBar name="Биографический"/>
                  <SideBar name="Боевик"/>
                  <SideBar name="Вестерн"/>
                  <SideBar name="Военный"/>
                  <SideBar name="Детектив"/>
                  <SideBar name="Детский"/>
                  <SideBar name="Документальный"/>
                  <SideBar name="Драма"/>
                  <SideBar name="Исторический"/>
                  <SideBar name="Кинокомикс"/>
                  <SideBar name="Концерт"/>
                  <SideBar name="Короткометражный"/>
                  <SideBar name="Криминал"/>
                  <SideBar name="Мелодрама"/>
                  <SideBar name="Мистика"/>
                  <SideBar name="Музыка"/>
                  <SideBar name="Мультфильм"/>
                  <SideBar name="Мюзикл"/>
                  <SideBar name="Научный"/>
                  <SideBar name="Приключения"/>
                  <SideBar name="Реалити-шоу"/>
                  <SideBar name="Семейный"/>
                  <SideBar name="Спорт"/>
              </div>
              <div className="movies-block">
              <Movie movies="Черная Пантера" year="2018" img={img01} />
              <Movie movies="Мстители: Война бесконечности" year="2018" img={img02}/>
              <Movie movies="Тихое место" year="2018" img={img03}/>
              <Movie movies="Миссия невыполнима: Последствия" year="2018" img={img04}/>
              <Movie movies="Хроники хищных городов" year="2018" img={img05}/>
              <Movie movies="Щелкунчик и четыре королевства" year="2018" img={img06}/>
              </div>

          </div>

          <Footer/>
      </div>
    );
  }
}


export default App;
