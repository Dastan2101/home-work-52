import React from 'react';


const Movie = (props) => {
    return  (
        <div className="div-movie">
            <img src={props.img} alt=""></img>
            <h5>{props.movies} {props.year}</h5>
        </div>
    )
};

export default Movie;