import React from 'react';

const Header = () => {
    return (
        <header>
            <ul className="main-nav">
                <li className="link-nav">IMAX</li>
                <li className="link-nav">DOLBY ATMOS</li>
                <li className="link-nav">Фильмы</li>
                <li className="link-nav">Анонсы</li>
            </ul>
        </header>
    )
};

export default Header;