import React from 'react';

const Footer = () => {
    return (
        <footer>
            <ul className="footer-nav">
                <li className="link-nav">Политика конфиденциальности</li>
                <li className="link-nav">Контакты</li>
                <li className="link-nav">Свяжитесь с нами</li>
                <li className="link-nav">Рассылка</li>
                <li className="link-nav">Уведомления о рекламе</li>
            </ul>
        </footer>
    )
};

export default Footer;
